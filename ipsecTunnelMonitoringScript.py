#! /usr/bin/python3 -B

# Copyright (C) 2018  Noel Kuntze <noel.kuntze@thermi.consulting> for VINN GmbH
# Copyright (C) 2020  Carl-Daniel Hailfinger
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import logging
import sys
import threading
import vici
import yaml

class tunnelChecker():
    """
    This class implements the monitoring and reestablishing of the listed tunnels
    """
    def __init__(self, tunnels = [], socketPath = None, configFile = None, verbose = False, quiet = False):
        """
        param tunnels: List of tunnels (as strings). Those are the names of the configured CHILD_SAs.
        param socketPath: If a non-standard socket location is used, set this argument to the path to it.
        """
        self.tunnels = []
        self.loadConfiguration(configFile)

        if not len(tunnels) > 0 and not self.tunnels:
            logging.error("No tunnels to monitor were configured.")
            sys.exit(1)
        self.session = vici.Session(socketPath)

        self.tunnels.extend(tunnels)

        self.logging = logging.getLogger(__name__)
        channel = logging.StreamHandler()
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        channel.setFormatter(formatter)
        self.logging.addHandler(channel)
        self.verbose = verbose
        self.quiet = quiet

        self.logging.setLevel(logging.INFO)
        channel.setLevel(logging.INFO)

        if self.verbose:
            self.logging.setLevel(logging.DEBUG)
            channel.setLevel(logging.DEBUG)

        if self.quiet:
            self.logging.setLevel(logging.ERROR)
            channel.setLevel(logging.ERROR)

    def loadConfiguration(self, path):
        try:
            file=None
            try:
                file = open(path, "r")
            except Exception as e:
                logging.info("Could not open the configuration file. Not loading tunnels from it. Exception: %s", e)
                return

            yaml_object = yaml.safe_load(file)
            if not isinstance(yaml_object, list):
                logging.critical("The content of the configuration file has to be a list.")
                sys.exit(2)
            for element in yaml_object:
                if not isinstance(element, str):
                    logging.critical("The list must only contain strings. Invalid item: %s", element)
                    sys.exit(3)
            self.tunnels.extend(yaml_object)
        except Exception as e:
            logging.critical("A critical exception occured while reading from the configuration file: %s". e)
            sys.exit(4)
        return

    def terminate_tunnels(self, child_sa_names):
        def terminate_tunnel(child_sa_name_to_terminate, logger, failed_tunnels, rlock):
            session = vici.Session()
            logger.warning("Deleting CHILD_SA {}".format(child_sa_name_to_terminate))
            try:
                for message in session.terminate(
                    {
                        "child-id" : str(child_sa_name_to_terminate)
                    }):
                    logger.debug("TERMINATE CHILD_SA {}: {}".format(child_sa_name_to_terminate, message))
                logger.warning("Terminated tunnel successfully {}".format(child_sa_name_to_terminate))
            except Exception as e:
                logger.critical("Exception while terminating tunnel {}: {}".format(child_sa_name_to_terminate, e))
                with rlock:
                    failed_tunnels.append(child_sa_name_to_terminate)

        threads = []
        failed_tunnels = []
        rlock = threading.RLock()
        for child_sa_name in child_sa_names:
            newThread = threading.Thread(target=terminate_tunnel, args=(child_sa_name, self.logging, failed_tunnels, rlock))
            threads.append(newThread)
            newThread.start()

        for thread in threads:
            thread.join()
        if len(failed_tunnels) == 0:
            return True
        else:
            self.logging.error("Failed to terminate the following tunnels: {}".format(" ".join(failed_tunnels)))
            return False

    def initiate_tunnels(self, child_sa_names):
        def initiate_tunnel(child_sa_name, logger, failed_tunnels, rlock):
            session = vici.Session()
            logger.warning("Initiating tunnel {}".format(child_sa_name))
            try:
                for message in session.initiate(
                    {
                        "child" : str(child_sa_name)
                    }):
                    logger.debug("INIT CHILD_SA {}: {}".format(child_sa_name, message))
                logger.warning("Initiated tunnel successfully {}".format(child_sa_name))
            except Exception as e:
                logger.critical("Exception while initiating tunnel {}: {}".format(child_sa_name, e))
                with rlock:
                    failed_tunnels.append(child_sa_name)

        threads = []
        failed_tunnels = []
        rlock = threading.RLock()
        for child_sa_name in child_sa_names:
            new_thread = threading.Thread(target=initiate_tunnel, args=(child_sa_name, self.logging, failed_tunnels, rlock))
            threads.append(new_thread)
            new_thread.start()

        for thread in threads:
            thread.join()
        if len(failed_tunnels) == 0:
            return True
        
        self.logging.error("Failed to initiate the following tunnels: %s", " ".join(failed_tunnels))
        return False

    def main(self):
        # list SAS
        down_tunnels=list(self.tunnels)
        duplicate_child_sas = []
        for i in self.session.list_sas():
            # iterate over the values. The name of the IKE_SAs is irrelevant.
            for j in i.values():
                uniquechildsas = []
                # in different versions of strongSwan, the keys of the items is a unique string composed
                # of the name of the CHILD_SA configuration and the CHILD_SA's ID.
                # example: foo-5
                # Older versions return the key as the CHILD_SA config's name. We need to catch both.
                for child_sa_name, child_sa_value in j["child-sas"].items():
                    # check if the name is as in older versions
                    # value is of type bytes, so we need to convert to string first
                    filtered_child_sa_value = {k: v for k, v in child_sa_value.items() if k in ("name", "reqid", "state", "mode", "protocol", "encr-alg", "encr-keysize", "integ-alg", "local-ts", "remote-ts")}
                    if filtered_child_sa_value in uniquechildsas:
                        self.logging.debug("duplicate CHILD_SA found: uniqueid %s common values %s", child_sa_value["uniqueid"], filtered_child_sa_value)
                        duplicate_child_sas.append(str(child_sa_value["uniqueid"], "UTF-8"))
                    else:
                        uniquechildsas.append(filtered_child_sa_value)
                    state = str(child_sa_value["state"], "UTF-8")
                    if isinstance(child_sa_name, bytes):
                        name = str(child_sa_name, "UTF-8")
                    else:
                        name = child_sa_name
                    if name in down_tunnels and state == "INSTALLED":
                        down_tunnels.remove(child_sa_name)
                        continue
                    # check if this is as in newer versions
                    last_dash = name.rfind("-")
                    # dash found
                    if last_dash > 0:
                        # check if anything after the dash is numbers
                        if name[last_dash+1:].isnumeric():
                            # now check the rest
                            rest = name[:last_dash]
                            if rest in down_tunnels and state == "INSTALLED":
                                down_tunnels.remove(rest)
                    # no Dash found and no match

        if len(down_tunnels) > 0:
            self.logging.warning("The following tunnels were detected as being down: %s", " ".join(down_tunnels))

            if self.initiate_tunnels(down_tunnels):
                self.logging.info("All tunnels up.")
                sys.exit(0)
            else:
                sys.exit(1)
        if len(duplicate_child_sas) > 0:
            self.logging.warning("The following CHILD_SA were detected as being duplicate: %s", " ".join(duplicate_child_sas))
            if self.terminate_tunnels(duplicate_child_sas):
                self.logging.info("Duplicate tunnels removed.")
                sys.exit(0)
            else:
                sys.exit(1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="A script to monitor and reestablish tunnels.")
    parser.add_argument("--socket",
        help="If a non-standard path to the VICI socket is used, the path to it is set with this argument.",
        type=str,
        default=None)
    parser.add_argument("--config",
        help="The path to the optional configuration file.",
        type=str,
        default="/etc/ipsecTunnelMonitoringScript.yml")
    parser.add_argument("--verbose",
        help="Print additional information",
        action="store_true",
        default=False,)
    parser.add_argument("--quiet",
        help="Disable any non-error output from the script",
        action="store_true",
        default=False)
    parser.add_argument("tunnels",
        nargs="*",
        help="The list of CHILD_SAs to monitor and reestablish")

    args = parser.parse_args()

    if args.verbose and args.quiet:
        logging.error("--verbose and --quiet can not be used at the same time.")
        sys.exit(1)

    checker = tunnelChecker(args.tunnels, args.socket, args.config, args.verbose, args.quiet)
    checker.main()
